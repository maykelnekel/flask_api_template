from flask import Flask
from flask_migrate import Migrate

def init_app(app: Flask):
    
    # IMPORTE AQUI SUAS CLASSES

    Migrate(app, app.db)
